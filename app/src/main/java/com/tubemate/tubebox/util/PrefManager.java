package com.tubemate.tubebox.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    private static final String PREF_NAME = "TubeBox";
    private static final int PRIVATE_MODE = 0;

    public static final String FIRST_TIME_LAUNCH = "FirstTimeLaunch";

    Context context;
    SharedPreferences preferences;

    public PrefManager(Context context) {
        this.context = context;
        preferences  = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public void putBoolean(String keyword, Boolean value){
        preferences.edit().putBoolean(keyword, value).apply();
    }

    public Boolean getBoolean(String keyword, Boolean value){
        return preferences.getBoolean(keyword,value);
    }

}
