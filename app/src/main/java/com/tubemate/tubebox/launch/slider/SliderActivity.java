package com.tubemate.tubebox.launch.slider;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.tubemate.tubebox.R;
import com.tubemate.tubebox.launch.auth.SigninActivity;
import com.tubemate.tubebox.launch.home.HomeActivity;
import com.tubemate.tubebox.util.BaseActivity;
import com.tubemate.tubebox.util.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SliderActivity extends BaseActivity {
    private PrefManager prefManager;

    private List<Slider> sliders;
    private int currentPage = 0;
    private TextSwitcher textSwitcher;
    private List<TextView> textViews;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        if(!prefManager.getBoolean(PrefManager.FIRST_TIME_LAUNCH, true)){
            startActivity(new Intent(SliderActivity.this, HomeActivity.class));
            finish();
        }

        setContentView(R.layout.activity_slider);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        sliders = new ArrayList<>();
        sliders.add(new Slider(R.drawable.s1,"Enjoy Blockbuster Movies from Bollywood, Tollywood and more...",R.color.slideText1));
        sliders.add(new Slider(R.drawable.s1_blur,"Listen to Hindi, Tamil, Telugu and Malayalam Music for Free",R.color.slideText2));
        sliders.add(new Slider(R.drawable.s1_light_dark,"Popular Indian & International TV Shows",R.color.slideText3));

        textViews = new ArrayList<>();
        textViews.add((TextView)findViewById(R.id.dot1));
        textViews.add((TextView)findViewById(R.id.dot2));
        textViews.add((TextView)findViewById(R.id.dot3));

        textSwitcher = findViewById(R.id.textView);

        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.slide_in_zoomin);
        final Animation leftout = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.slide_out_zoomout);
        final Animation leftin = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);

        textSwitcher.setInAnimation(zoomin);
        textSwitcher.setOutAnimation(leftout);

        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                // TODO Auto-generated method stub
                // create a TextView
                TextView t = new TextView(SliderActivity.this);
                // set the gravity of text to top and center horizontal
                t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                // set displayed text size
                t.setTextSize(26);
                t.setTextColor(getResources().getColor(R.color.colorText));
                Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/roboto_bold.ttf");
                t.setTypeface(custom_font);
                t.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                return t;
            }
        });

        textSwitcher.setCurrentText(sliders.get(currentPage++).getText());

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                textViews.get(currentPage++).setTextColor(getResources().getColor(R.color.colorText));
                if (currentPage == sliders.size()) {
                    currentPage = 0;
                }
                textViews.get(currentPage).setTextColor(getResources().getColor(R.color.colorAccent));
                textSwitcher.setText(sliders.get(currentPage).getText());
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);

        LinearLayout myImage = findViewById(R.id.root);
        myImage.setOnTouchListener(new RelativeLayoutTouchListener(this, new SwipeListener() {
            @Override
            public void swipeLeft() {
                if (currentPage != 0) {
                    textViews.get(currentPage--).setTextColor(getResources().getColor(R.color.colorText));
                    textViews.get(currentPage).setTextColor(getResources().getColor(R.color.colorAccent));
                    textSwitcher.setInAnimation(leftin);
                    textSwitcher.setOutAnimation(zoomout);
                    textSwitcher.setText(sliders.get(currentPage).getText());
                    textSwitcher.setInAnimation(zoomin);
                    textSwitcher.setOutAnimation(leftout);
                }
            }

            @Override
            public void swipeRight() {
                if (currentPage != sliders.size()-1) {
                    textViews.get(currentPage++).setTextColor(getResources().getColor(R.color.colorText));
                    textViews.get(currentPage).setTextColor(getResources().getColor(R.color.colorAccent));
                    textSwitcher.setText(sliders.get(currentPage).getText());

                }
            }
        }));

        findViewById(R.id.buttonSignin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SliderActivity.this,SigninActivity.class));
                finish();
            }
        });
    }

    public class RelativeLayoutTouchListener implements View.OnTouchListener {

        static final String logTag = "ActivitySwipeDetector";
        private final SwipeListener swipeListener;
        private Context activity;
        static final int MIN_DISTANCE = 100;// TODO change this runtime based on screen resolution. for 1920x1080 is to small the 100 distance
        private float downX, downY, upX, upY;

        public RelativeLayoutTouchListener(Context activity, SwipeListener swipeListener) {
            this.activity = activity;
            this.swipeListener = swipeListener;
        }

        public void onRightToLeftSwipe() {
            swipeListener.swipeRight();
        }

        public void onLeftToRightSwipe() {
            swipeListener.swipeLeft();
        }

        public void onTopToBottomSwipe() {

        }

        public void onBottomToTopSwipe() {

        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    downY = event.getY();
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    // swipe horizontal?
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        // left or right
                        if (deltaX < 0) {
                            this.onLeftToRightSwipe();
                            return true;
                        }
                        if (deltaX > 0) {
                            this.onRightToLeftSwipe();
                            return true;
                        }
                    } else {
                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long horizontally, need at least " + MIN_DISTANCE);
                        // return false; // We don't consume the event
                    }

                    // swipe vertical?
                    if (Math.abs(deltaY) > MIN_DISTANCE) {
                        // top or down
                        if (deltaY < 0) {
                            this.onTopToBottomSwipe();
                            return true;
                        }
                        if (deltaY > 0) {
                            this.onBottomToTopSwipe();
                            return true;
                        }
                    } else {
                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long vertically, need at least " + MIN_DISTANCE);
                        // return false; // We don't consume the event
                    }

                    return false; // no swipe horizontally and no swipe vertically
                }// case MotionEvent.ACTION_UP:
            }
            return false;
        }

    }

    private interface SwipeListener {
        void swipeLeft();
        void swipeRight();
    }
}
