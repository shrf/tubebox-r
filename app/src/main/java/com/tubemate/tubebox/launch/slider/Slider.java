package com.tubemate.tubebox.launch.slider;

class Slider {
    private int image;
    private String text;
    private int textColor;

    public Slider(int image, String text, int textColor) {
        this.image = image;
        this.text = text;
        this.textColor = textColor;
    }

    public int getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    public int getTextColor() {
        return textColor;
    }
}
